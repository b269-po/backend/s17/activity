/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printWelcomeMessage(){
	let fullName = prompt("Enter your full name: ");
	let Age = prompt("Enter your Age: ");
	let Location = prompt("Enter your location: ");

	console.log("Hello, " + fullName);
	console.log("You are " + Age + " " + "years old");
	console.log("You live in " + Location);
}

printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function faveBand(){
	let firstBand = "Fall Out Boy";
	let secondBand = "Taking Back Sunday";
	let thirdBand = "FM Static";
	let fourthBand = "All Time Low";
	let fifthBand = "We The Kings";

	console.log("1. " + firstBand);
	console.log("2. " + secondBand);
	console.log("3. " + thirdBand);
	console.log("4. " + fourthBand);
	console.log("5. " + fifthBand);
}

faveBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function movieRating(){
	let firstMovie = "Batman Begins";
	console.log("1. " + firstMovie);
	console.log("Rotten Tomatoes Ratings: 84%")
	let secondMovie = "The Dark Knight";
	console.log("2. " + secondMovie);
	console.log("Rotten Tomatoes Ratings: 94%")
	let thirdMovie = "The Dark Knight Rises";
	console.log("3. " + thirdMovie);
	console.log("Rotten Tomatoes Ratings: 87%")
	let fourthMovie = "The Batman 2022";
	console.log("4. " + fourthMovie);
	console.log("Rotten Tomatoes Ratings: 85%")
	let fifthMovie = "The Lego Batman Movie";
	console.log("5. " + fifthMovie);
	console.log("Rotten Tomatoes Ratings: 90%")	
	
}
movieRating();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}
printFriends();


// console.log(friend1);
// console.log(friend2);
